﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustNumbers
{
    class Program
    {
        static List<int> intList = new List<int>();

        private static double CalculateStdDev(List<int> values)
        {
            double ret = 0;
            if (!values.Any()) return ret;
            //Compute the Average      
            double avg = values.Average();
            //Perform the Sum of (value-avg)_2_2      
            double sum = values.Sum(d => Math.Pow(d - avg, 2));
            //Put it all together      
            ret = Math.Sqrt((sum) / (values.Count() - 1));
            return ret;
        }

        static void Main(string[] args)
        {
            while (true)
            {
                Console.Write("Enter an integer positive number (0 to finish): ");
                string line = Console.ReadLine();
                int num;
                if (!Int32.TryParse(line, out num))
                {
                    Console.WriteLine("Invalid entry, try again");
                    continue;
                }
                if (num <= 0)
                {
                    break;
                }
                intList.Add(num);
            }
            // average
            double sum = 0;
            foreach (int n in intList)
            {
                sum += n;
            }
            double average = sum / intList.Count;
            Console.WriteLine("Average is {0}", average.ToString("#.##"));
            //            
            var max = intList.Max();
            Console.WriteLine("Max is {0}", max);
            //
            intList.Sort();
            double median;
            if (intList.Count % 2 == 1)
            { // odd  number - take middle
                median = intList[intList.Count / 2];
            }
            else
            { // even number - average two middle ones
                var right = intList[intList.Count / 2];
                var left = intList[intList.Count / 2 - 1];
                median = ((double)right + left) / 2;
            }
            Console.WriteLine("Median in {0}", median);

            Console.WriteLine("The standard deviation is: {0}", CalculateStdDev(intList).ToString("#.##"));
            // TODO: compute median

            // output list to file
            try
            {
                StreamWriter file = new StreamWriter(@"K:\..\..\output.txt");
                String dataLine = String.Join(";", intList);
                file.Write(dataLine);
                file.Close();
            }
            catch (IOException e)
            {
                Console.WriteLine("FATAL ERROR: " + e.StackTrace);
            }
            //
            Console.ReadKey();
        }
    }
}


