﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace StudentRenaissance
{
    internal class Student
    {
        public string Name { get; set; }

        public double Gpa { get; set; }

        public Student(string name, double gpa)
        {
            Name = name;
            Gpa = gpa;
        }

        public override string ToString()
        {
            string s = string.Format("{0} : {1:0.0}", Name,Gpa);
            return s;
        }
    }

    internal class Program
    {
        static List<Student> StudentList = new List<Student>();

        private static void Main(string[] args)
        {
            int choice;
            LoadDataFromFile();
            do
            {
                choice = GetMenuChoice();
                switch (choice)
                {
                    case 1:
                        AddStudent();
                        break;
                    case 2:
                        ListStudent();
                        break;
                    case 3:
                        FindStudentByLetter();
                        break;
                    case 4:
                        FindAverageGpa();
                        break;
                    case 0: break;
                    default:
                        Console.WriteLine("Invalid choice");
                        break;
                }
            } while (choice != 0);

            SaveDataToFile();
            DisplayFile();
            Console.WriteLine("Good Bye!");
            Console.ReadKey();
        }

        static int GetMenuChoice()
        {
            do
            {
                Console.WriteLine("1. Add student and their GPA");
                Console.WriteLine("2. List all students and their GPAs");
                Console.WriteLine("3. Find all students whose name begins with a letter");
                Console.WriteLine("4. Find the average GPA of all students and display it");
                Console.WriteLine("0. Exit");
                Console.Write("Choice: ");

                string choice = Console.ReadLine();
                int option;
                if (!Int32.TryParse(choice, out option))
                    Console.WriteLine("Invalid input, try again");
                else if (option < 0 || option > 4)
                    Console.WriteLine("Invalid input, try again");
                else return option;

            } while (true);
        }

        static void LoadDataFromFile()
        {
            try
            {
                string[] lineArray = File.ReadAllLines(@"..\..\students.txt");
                foreach (string line in lineArray)
                {
                    string[] s1 = line.Split(':');

                    string name = s1[0];
                    double gpa;
                    if (!Double.TryParse(s1[1], out gpa))
                        Console.WriteLine("Invalid GPA data " + gpa);
                    Student s = new Student(name, gpa);
                    StudentList.Add(s);
                    //
                    Console.WriteLine("{0} has GPA {1:0.0}", name, gpa);
                }
            }
            catch (IOException e)
            {
                Console.WriteLine("FATAL ERROR: " + e.StackTrace);
            }
        }

        static void SaveDataToFile()
        {
            try
            {
                StreamWriter file = new StreamWriter(@"..\..\students.txt");
                foreach (Student s in StudentList)
                {
                    file.WriteLine(s.ToString());
                }
                //File.WriteAllLines(@"..\..\students.txt", lineArray);
                file.Close();
            }
            catch (IOException e)
            {
                Console.WriteLine("FATAL ERROR: " + e.StackTrace);
            }
        }

        static void AddStudent()
        {
            double gpa;

            Console.Write("Enter student's name:");
            string name = Console.ReadLine();
            do
            {
                Console.Write("Enter student's GPA:");
                string strGpa = Console.ReadLine();
                if (!Double.TryParse(strGpa, out gpa))
                {
                    Console.WriteLine("Invalid input, try again");
                }
                else if (gpa < 0 || gpa > 4.3)
                {
                    Console.WriteLine("GPA must be between 0 and 4.3 maximum.");
                }
                else break;
            } while (true);

            Student s = new Student(name, gpa);
            StudentList.Add(s);
            Console.WriteLine("Student added");
        }

        static void ListStudent()
        {
            foreach (Student s in StudentList)
            {
                Console.WriteLine("{0} has GPA {1:0.0}", s.Name, s.Gpa);
            }
        }

        static void FindStudentByLetter()
        {
            string letter;
            while (true)
            {
                Console.Write("Enter first letter of student name (only one letter):");
                letter = Console.ReadLine();
                if (letter != null && letter.Length > 1)
                {
                    Console.WriteLine("Invalid input, try again");
                }
                else if (letter != null && (Char.ToUpper(Convert.ToChar(letter)) < 'A' || Char.ToUpper(Convert.ToChar(letter)) > 'Z')) {
                    Console.WriteLine("Invalid input, your should input letter, try again");
                }
                else break;
            }

            int count = 0;
            foreach (Student s in StudentList)
            {
                if (letter != null && Char.ToUpper((s.Name[0])) == Char.ToUpper(Convert.ToChar(letter)))
                {
                    count++;
                    Console.WriteLine("{0} has GPA {1:0.0}", s.Name, s.Gpa);
                }
            }

            if (count == 0)
            {
                Console.WriteLine("No student found");
            }
        }

        static void FindAverageGpa()
        {
            double sum = 0;
            foreach (Student s in StudentList)
            {
                sum += s.Gpa;
            }
            double gpa = sum / StudentList.Count();
            Console.WriteLine("The average GPA of all students is {0:0.0}", gpa);
            Console.WriteLine("If you would like to save it to a file, provide file name (empty to skip):");
            string filename = Console.ReadLine();
            filename = "..\\..\\" + filename;
            try
            {
                string s = String.Format("{0:0.0}",gpa);
                File.WriteAllText(filename, s);
                Console.WriteLine("File saved");
            }
            catch (IOException e)
            {
                Console.WriteLine("FILE SAVE ERROR: " + e.StackTrace);
            }
        }

        static void DisplayFile()
        {
            Console.WriteLine("Contents of \"students.txt\" file after the above session will be:");
            try
            {
                string[] lineArray = File.ReadAllLines(@"..\..\students.txt");
                foreach (string line in lineArray)
                {
                    Console.WriteLine(line);
                }
            }
            catch (IOException e)
            {
                Console.WriteLine("FATAL ERROR: " + e.StackTrace);
            }

            Console.WriteLine("Contents of \"avg.txt\" file after the above session will be:");
            try
            {
                string[] lineArray = File.ReadAllLines(@"..\..\avg.txt");
                foreach (string line in lineArray)
                {
                    Console.WriteLine(line);
                }
            }
            catch (IOException e)
            {
                Console.WriteLine("FATAL ERROR: " + e.StackTrace);
            }
        }
    }
}
