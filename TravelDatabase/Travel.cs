﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelDatabase
{
    class Travel
    {
        private string name;
        private string passport;
        private string destination;
        private string depature;

        public Travel(string name, string passport, string destination, string depature)
        {
            this.name = name;
            this.passport = passport;
            this.destination = destination;
            this.depature = depature;
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Passport
        {
            get { return passport; }
            set { passport = value; }
        }

        public string Destination
        {
            get { return destination; }
            set { destination = value; }
        }

        public string Depature
        {
            get { return depature; }
            set { depature = value; }
        }
    }
}
