﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Quiz1BankingRecords
{
    internal class Program
    {
        static List<AccountTransaction> transactionList = new List<AccountTransaction>();

        private static void Main(string[] args)
        {
            LoadDataFromFile();
            ListAccount();
            CalcBalance();
            Console.WriteLine("Good Bye!");
            Console.ReadKey();
        }

        static void LoadDataFromFile()
        {
            try
            {
                string[] lineArray = File.ReadAllLines(@"..\..\operations.txt");
                foreach (string line in lineArray)
                {
                    AccountTransaction t = new AccountTransaction(line);
                }
            }
            catch (IOException e)
            {
                Console.WriteLine("FATAL ERROR: " + e.StackTrace);
            }
        }

        static void ListAccount()
        {
            foreach (AccountTransaction t in transactionList)
            {
                Console.WriteLine("{0:d} {1} {2:0.00} {3:0.00}", t.date.ToString("yyyy-MM-dd"), t.description, t.deposit,t.withdrawal);
            }
        }

        static void CalcBalance()
        {
            decimal balance = 0;
            foreach (AccountTransaction t in transactionList)
            {
                balance += t.deposit;
                balance -= t.withdrawal;
            }

            Console.WriteLine("The balance is {0:0.00}", balance);
        }

        public class AccountTransaction
        {
            public readonly DateTime date;
            public readonly string description;
            public readonly decimal deposit;
            public readonly decimal withdrawal;

            public AccountTransaction(string line)
            {
                string[] s1 = line.Split(';');

                DateTime date;
                if (!DateTime.TryParse(s1[0], out date))
                {
                    throw new FileFormatException("Invalid date" + date);
                }

                string description = s1[1];
                if (string.IsNullOrEmpty(description) || description.Length < 2)
                {
                    Console.WriteLine("description is at least 2 characters long");
                    throw new FileFormatException("description is at least 2 characters long" + description);
                }

                decimal deposit;
                if (!Decimal.TryParse(s1[2], out deposit))
                {
                    Console.WriteLine("Invalid deposit data ");
                }
                else if (deposit < 0)
                {
                    Console.WriteLine("deposit is non-negative values");
                    throw new FileFormatException("deposit is non-negative values" + date);
                }

                decimal withdrawal;
                if (!Decimal.TryParse(s1[3], out withdrawal))
                {
                    Console.WriteLine("Invalid withdrawal data ");
                    throw new FileFormatException("Invalid withdrawal date" + date);
                }
                    
                else if (withdrawal < 0)
                {
                    Console.WriteLine("withdrawal is non-negative values ");
                    throw new FileFormatException("withdrawal is non-negative values" + date);
                }

                if ((deposit == 0) && (withdrawal == 0) || (deposit != 0) && (withdrawal != 0))
                    throw new FileFormatException("either deposit or withdrawal must be 0" + deposit + withdrawal);

                AccountTransaction t = new AccountTransaction(date, description, deposit, withdrawal);
                transactionList.Add(t);
            }

            public AccountTransaction(DateTime date, string description, decimal deposit, decimal withdrawal)
            {
                this.date = date;
                this.description = description;
                this.deposit = deposit;
                this.withdrawal = withdrawal;
            }
        }
    }
}    