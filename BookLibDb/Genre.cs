﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookLibDb
{
    class Genre
    {
        public Genre(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public int Id;
        public string Name { get; set; }

        public static int GenreToIndex(string genre)
        {
            switch (genre)
            {
                case "Computer":        return 1;
                case "Fantasy":         return 2;
                case "Romance":         return 3;
                case "Horror":          return 4;
                case "Science Fiction": return 5;
                default: throw new ArgumentOutOfRangeException("Unknown grade" + genre);
            }
        }

    }

}

