﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Linq;
using Microsoft.Win32;

namespace BookLibDb
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Database db;

        public MainWindow()
        {
            try
            {
                db = new Database();
                InitializeComponent();
                RefreshBookList();
                // TODO: load genres into combo box
                var genreList = db.GetAllGenres();
                foreach (var g in genreList)
                {
                    CbGenre.Items.Add(g.Name);
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.StackTrace);
                MessageBox.Show("Error opening database connection: " + e.Message);
                Environment.Exit(1);
            }
        }

        private void RefreshBookList()
        {
            lvBookList.ItemsSource = db.GetAllBooks();
            ResetAllFields();
        }

        private void TbFilter_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            string filter = tbFilter.Text.ToLower();
            if (filter == "")
            {
                lvBookList.ItemsSource = db.GetAllBooks();
            }
            else
            {
                List<Book> list = db.GetAllBooks();
                /* var filteredList = list.Where(b => b.Title.ToLower().Contains(filter)
                                                   || b.Author.ToLower().Contains(filter)); */
                var filteredList = from b in list where b.Title.ToLower().Contains(filter) || b.Author.ToLower().Contains(filter) select b;

                lvBookList.ItemsSource = filteredList;
            }
        }

        private void BtUpdate_Click(object sender, RoutedEventArgs e)
        {
            int index = lvBookList.SelectedIndex;
            if (index < 0)
            {
                return;
            }
            Book b = (Book)lvBookList.Items[index];
            try
            {
                
                b.Author = TbAuthor.Text;
                b.Title = TbTitle.Text;
                b.GenreId = CbGenre.SelectedIndex + 1;
                b.Price = Decimal.Parse(TbPrice.Text);
                b.PublishDate = DateTime.Parse(TbPubDate.Text);
                b.Description = TbDescription.Text;
                db.UpdateBook(b);
                RefreshBookList();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Database update error " + ex.Message);
            }
        }

        private void BtAdd_Click(object sender, RoutedEventArgs e)
        {
            string author = null, title = null, description = null;
            DateTime publishDate = new DateTime();
            int genreId = 0;
            Decimal price = 0;

            try
            {
                genreId = CbGenre.SelectedIndex + 1;
                if (TbAuthor.Text == "")
                {
                    MessageBox.Show("Author cannot be null");
                    return;
                }
                else
                {
                    author = TbAuthor.Text;
                }
                if (TbTitle.Text == "")
                {
                    MessageBox.Show("Title cannot be null");
                    return;
                }
                else
                {
                    title = TbTitle.Text;
                }
                if (TbPrice.Text == "")
                {
                    MessageBox.Show("Price cannot be null");
                    return;
                }
                else
                {
                    price = Decimal.Parse(TbPrice.Text);
                }
                if (TbPubDate.Text == "")
                {
                    MessageBox.Show("Price cannot be null");
                    return;
                }
                else
                {
                    publishDate = DateTime.Parse(TbPubDate.Text);
                }

                if (TbDescription.Text == "")
                {
                    MessageBox.Show("Description cannot be null");
                    return;
                }
                else
                {
                    description = TbDescription.Text;
                }

                db.AddBook(new Book(1, genreId, author, title, price, publishDate, description));
                RefreshBookList();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Database insert error " + ex.Message);
            }
        }

        private void BtDelete_Click(object sender, RoutedEventArgs e)
        {
            int index = lvBookList.SelectedIndex;
            if (index < 0)
            {
                return;
            }
            Book b = (Book)lvBookList.Items[index];
            try
            {
                db.DeleteBookById(b.Id); ;
                RefreshBookList();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Database delete error " + ex.Message);
            }
        }

        void ResetAllFields()
        {
            TbAuthor.Text = "";
            TbTitle.Text = "";
            CbGenre.SelectedIndex = 0;
            TbPrice.Text = "";
            TbPubDate.Text = "";
            TbDescription.Text = "";

        }

        private void MenuItemImport_OnClick(object sender, RoutedEventArgs e)
        {
            string author, title, description;
            DateTime publishDate;
            Decimal price;
            int genreId;

            try
            {
                var openFileDialog = new OpenFileDialog
                {
                    Filter = "Text file (*.xml)|*.xml",
                    InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
                };
                if (openFileDialog.ShowDialog() == true)
                {
                    var xml = XDocument.Load(openFileDialog.FileName);

                    // Query the data and write out a subset of contacts
                    if (xml.Root != null)
                    {
                        var query = from b in xml.Root.Descendants("book") select b;

                        foreach (var b in query)
                        {
                            var xElement = b.Element("author");
                            if (xElement != null)
                            {
                                author = xElement.Value;
                                Console.WriteLine(author);
                            }
                            else
                            {
                                MessageBox.Show("Invalid author");
                                continue;
                            }
                            xElement = b.Element("title");
                            if (xElement != null)
                            {
                                title = xElement.Value;
                                Console.WriteLine(title);
                            }
                            else
                            {
                                MessageBox.Show("Invalid title");
                                continue;
                            }
                            xElement = b.Element("genre");
                            if (xElement != null)
                            {
                                genreId = Genre.GenreToIndex(xElement.Value);
                                Console.WriteLine(genreId);
                            }
                            else
                            {
                                MessageBox.Show("Invalid genre");
                                continue;
                            }
                            xElement = b.Element("price");
                            if (xElement != null)
                            {
                                Decimal.TryParse(xElement.Value, out price);
                                Console.WriteLine(price);
                            }
                            else
                            {
                                MessageBox.Show("Invalid price");
                                continue;
                            }
                            xElement = b.Element("publish_date");
                            if (xElement != null)
                            {
                                DateTime.TryParse(xElement.Value,out publishDate);
                                Console.WriteLine(publishDate);
                            }
                            else
                            {
                                MessageBox.Show("Invalid publishDate");
                                continue;
                            }
                            xElement = b.Element("description");
                            if (xElement != null)
                            {
                                description = xElement.Value;
                                Console.WriteLine(description);
                            }
                            else
                            {
                                MessageBox.Show("Invalid description");
                                continue;
                            }
                            try
                            {
                                db.AddBook(new Book(1, genreId, author, title, price, publishDate, description));
                                RefreshBookList();
                            }
                            catch (SqlException ex)
                            {
                                Console.WriteLine(ex.StackTrace);
                                MessageBox.Show("Database insert error " + ex.Message);
                            }
                        }
                    }
                }
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("File not found" + ex.StackTrace);
            }
        }

        private void MenuItemExit_OnClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void LvBookList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = lvBookList.SelectedIndex;
            if (index < 0)
            {
                return;
            }
            Book b = (Book)lvBookList.Items[index];
            TbAuthor.Text = b.Author;
            TbTitle.Text = b.Title;
            CbGenre.SelectedIndex = b.GenreId - 1;
            TbPrice.Text = b.Price.ToString("0.00",CultureInfo.InvariantCulture);
            TbPubDate.Text = b.PublishDate.ToString("yyyy-MM-dd",CultureInfo.InvariantCulture);
            TbDescription.Text = b.Description;
        }

    }
}
