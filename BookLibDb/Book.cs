﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookLibDb
{
    class Book
    {
        public Book(int id, int genreId, string author, string title,  decimal price, DateTime publishDate, string description)
        {
            Id = id;
            GenreId = genreId;
            Author = author;
            Title = title;
            Description = description;
            PublishDate = publishDate;
            Price = price;
        }
        
        public int Id { get; set; }
        public int GenreId;
        public string Author { get; set; }
        public string Title { get; set; }
        public string Description;
        public DateTime PublishDate;
        public Decimal Price;
    }
}
