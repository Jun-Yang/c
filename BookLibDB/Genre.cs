﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookLibDB
{
    class Genre
    {
        private int id;
        private string genre;

        public Genre(int id, string genre)
        {
            this.id = id;
            this.genre = genre;
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Genre1
        {
            get { return genre; }
            set { genre = value; }
        }
    }
}
