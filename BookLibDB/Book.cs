﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookLibDB
{
    class Book
    {
        private int id;
        private string author, title;

        public string Author
        {
            get { return author; }
            set { author = value; }
        }

        public string Title
        {
            get { return title; }
            set { title = value; }
        }

        private DateTime publishDate;
        private string Description;
        private decimal price;
        private Genre genre;

        public Book(int id, string author, string title, DateTime publishDate, string description, decimal price, Genre genre)
        {
            this.id = id;
            this.author = author;
            this.title = title;
            this.publishDate = publishDate;
            this.Description = description;
            this.price = price;
            this.genre = genre;
        }

        public DateTime PublishDate
        {
            get { return publishDate; }
            set { publishDate = value; }
        }

        public string Description1
        {
            get { return Description; }
            set { Description = value; }
        }

        public decimal Price
        {
            get { return price; }
            set { price = value; }
        }

        public Genre Genre
        {
            get { return genre; }
            set { genre = value; }
        }
    }
}
