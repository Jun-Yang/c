﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentsGrades
{
    class Program
    {
        public static double LetterToNumberGrade(string strGrade)
        {
            switch (strGrade) {
                case "A+": return 4.00;
                case "A":  return 3.67;
                case "A-": return 3.33;
                case "B+": return 3.00;
                case "B":  return 2.67;
                case "B-": return 2.33;
                case "C+": return 2.00;
                case "C":  return 1.67;
                case "C-": return 1.33;
                case "D+": return 1.00;
                case "D":  return 0.67;
                case "D-": return 0.33;
                case "F":  return 0.00;
                default: throw new ArgumentOutOfRangeException("Unknown grade" + strGrade);
            }
        }
   
        static void Main(string[] args)
        {
            // read the string from a file.
            try
            {
                FileStream fileStream = new FileStream(@"..\..\grades.txt", FileMode.Open, FileAccess.Read);
                StreamReader streamReader = new StreamReader(fileStream, Encoding.UTF8);
                {
                    string line;
                    while ((line = streamReader.ReadLine()) != null)
                    {
                        char[] splitChars = new[] { ':', ',' };
                        string[] subStr = line.Split(splitChars);
                        double sum = 0;
                        for (int i = 1; i < subStr.Count(); i++)
                        {
                            sum += LetterToNumberGrade(subStr[i]);
                        }
                        //Console.WriteLine(subStr[0] + " has GPA " + (sum / (subStr.Count() - 1)).ToString("#.##"));
                        Console.WriteLine(" {0} has GPA {1:0.00}", subStr[0],(sum / (subStr.Count() - 1)));
                        Debug.WriteLine(subStr[0] + " has GPA " + (sum / (subStr.Count() - 1)).ToString("#.#"));
                    }
                    Console.ReadKey();
                }
            }
            catch (IOException e) {
                Console.WriteLine("Fatal Error" + e.StackTrace);
            }
        }
    }
}
