﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LocalPeopleDB
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Database db;

        public MainWindow()
        {
            try
            {
                db = new Database();
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.StackTrace);
                MessageBox.Show("Error opening database connection: " + e.StackTrace);
                Environment.Exit(1);
            }

            InitializeComponent();
            RefreshPeopleList();
        }

        private void RefreshPeopleList()
        {
            LvPerson.ItemsSource = db.GetAllPeople();
            LvPerson.Items.Refresh();
            ClearAllField();
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string name = tbName.Text;
                int age = int.Parse(tbAge.Text);

                Person p = new Person(name, age);
                db.AddPerson(p);
                RefreshPeopleList();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Error opening database connection: " + ex.StackTrace);
            }
        }

        private void ButtonUpdate_Click(object sender, RoutedEventArgs e)
        {
            int index = LvPerson.SelectedIndex;
            if (index < 0)
            {
                return;
            }
            string name = tbName.Text;
            string ageStr = tbAge.Text;
            string idStr = lblId.Content + "";
            int age;
            if (!int.TryParse(ageStr, out age))
            {
                MessageBox.Show("Age must be an integer");
                return;
            }

            int id;
            if (!int.TryParse(idStr, out id))
            {
                MessageBox.Show("Id must be an integer");
                return;
            }
            try
            {
                Person p = new Person(id, name, age);
                db.UpdatePerson(p);
                RefreshPeopleList();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Error opening database connection: " + ex.StackTrace);
            }
        }

        private void MenuItemDelete_Click(object sender, RoutedEventArgs e)
        {
            int index = LvPerson.SelectedIndex;
            if (index < 0)
            {
                return;
            }

            db.DeletePerson((Person) LvPerson.Items[index]);
            RefreshPeopleList();
        }

        private void LvPerson_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = LvPerson.SelectedIndex;
            if (index < 0)
            {
                lblId.Content = "...";
                return;
            }
            Person p = (Person) LvPerson.Items[index];
            lblId.Content = p.Id + "";
            tbName.Text = p.Name;
            tbAge.Text = p.Age + "";
        }

        void ClearAllField()
        {
            lblId.Content = "...";
            tbAge.Text = "";
            tbName.Text = "";
        }
    }
}
