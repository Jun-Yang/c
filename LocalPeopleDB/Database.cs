﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalPeopleDB
{
    class Database
    {
        private SqlConnection conn;

        public Database()
        {
            conn = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\C#\BookLibDb\LibraryDB.mdf;Integrated Security=True;Connect Timeout=30");
            conn.Open();
        }

        public void AddPerson(Person p)
        {
            string sql = "INSERT INTO people (Name,Age) VALUES (@Name,@Age)";
            SqlCommand cmd = new SqlCommand(sql,conn);
            cmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = p.Name;
            cmd.Parameters.Add("@Age", SqlDbType.Int).Value = p.Age;
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
        }

        public void UpdatePerson(Person p)
        {
            string sql = "UPDATE people SET Name=@Name, Age=@Age WHERE Id=@Id";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.Add("@Id", SqlDbType.Int).Value = p.Id;
            cmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = p.Name;
            cmd.Parameters.Add("@Age", SqlDbType.Int).Value = p.Age;
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
        }

        public void DeletePerson(Person p)
        {
            string sql = "DELETE FROM people WHERE Id=@Id";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.Add("@Id", SqlDbType.Int).Value = p.Id;
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
        }

        public List<Person> GetAllPeople()
        {
            List<Person> result = new List<Person>();
            string sql = "SELECT * FROM people";
            SqlCommand cmd = new SqlCommand(sql,conn);
            
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader = cmd.ExecuteReader();
            while(reader.Read())
            {
                Console.WriteLine("{0} {1} {2}", reader.GetInt32(0), reader.GetString(1),reader.GetInt32(2));
                int id = (int) reader["Id"];
                String name = (string) reader["Name"];
                int age = (int)reader["Age"];
                result.Add(new Person(id,name,age));
            }
            return result;
        }
    }
}
