﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PeopleBinding
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static List<Person> PersonList = new List<Person>();

        public MainWindow()
        {
            InitializeComponent();
        }

        public void AddPerson_Click(object sender, RoutedEventArgs e)
        {
            var name = TbName.Text;
            int age;
            Int32.TryParse(TbId.Text, out age);
            PersonList.Add(new Person(name,age));
        }

        void ResetAllField()
        {
            TbName.Text = "";
            TbAge.Text = "";
        }

        public void UpdatePerson_Click(object sender, RoutedEventArgs e)
        {
            int id;
            Int32.TryParse(TbId.Text, out id);
            PersonList.RemoveAt(id);
        }
    }
}
