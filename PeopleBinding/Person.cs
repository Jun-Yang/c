using System;
using System.IO;

namespace PeopleBinding
{
    class Person
    {
        public static int Id { get; private set; }

        public int Age
        {
            get { throw new NotImplementedException(); }
            private set
            {
                if (value <= 0) throw new ArgumentOutOfRangeException("value");
                if (Age < 1 || Age > 150) throw new InvalidDataException();
            }
        }

        internal string Name
        {
            get { throw new NotImplementedException(); }
            private set
            {
                if (value == null) throw new ArgumentNullException("value");
                if (Name.Length < 2 || Name.Length > 50) throw new InvalidDataException();
            }
        }
        
        public Person(string name, int age)
        {
            Name = name;
            Age = age;
            Id++;
        }
    }
}