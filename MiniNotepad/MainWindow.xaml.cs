﻿using Microsoft.Win32;
using System;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace MiniNotepad
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private bool _fileChanged;
        private string _filename = "c:\\Untitled.txt";

        public MainWindow()
        {
            InitializeComponent();
            TbFilePath.Text = "c:\\Untitled.txt";
        }

        private void txtEditor_SelectionChanged(object sender, RoutedEventArgs e)
        {

            int row = txtEditor.GetLineIndexFromCharacterIndex(txtEditor.CaretIndex);
            int col = txtEditor.CaretIndex - txtEditor.GetCharacterIndexFromLineIndex(row);
            lblCursorPosition.Text = "Line " + (row + 1) + ", Char " + (col + 1);
        }

        private void MiOpen_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var openFileDialog = new OpenFileDialog
                {
                    Filter = "Text file (*.txt)|*.txt|C# file (*.cs)|*.cs",
                    InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
                };
                if (openFileDialog.ShowDialog() == true)
                {
					XmlDocument doc = new XmlDocument();
					doc.Load(openFileDialog.FileName);
                }
                _fileChanged = false;
                TbFileStatus.Text = "";
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("File not found" + ex.StackTrace);
            }
        }

        private void MiSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                File.WriteAllText(_filename, txtEditor.Text);
                _fileChanged = false;
                TbFileStatus.Text = "";
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("File save error" + ex.StackTrace);
            }
        }

        private void MiSaveAs_Click(object sender, RoutedEventArgs e)
        {
            var saveFileDialog = new SaveFileDialog
            {
                Filter = "Text file (*.txt)|*.txt|C# file (*.cs)|*.cs",
                InitialDirectory = @"c:\temp\"
            };
            if (saveFileDialog.ShowDialog() != true) return;
            _filename = saveFileDialog.FileName;
            TbFilePath.Text = _filename;
            MiSave_Click(null, null);
        }

        private void MiExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void txtEditor_TextChanged(object sender, TextChangedEventArgs e)
        {
            _fileChanged = true;
            TbFileStatus.Text = "Modified";
        }


        private void Window_Closing(object sender, CancelEventArgs e)
        {
            if (!_fileChanged) return;
            var result = MessageBox.Show("Do you want to save changes to Untitled?", "MiniNotepad", MessageBoxButton.YesNoCancel);
            switch (result)
            {
                case MessageBoxResult.Yes:
                    MiSave_Click(null, null);
                    break;
                case MessageBoxResult.No:
                    break;
                case MessageBoxResult.Cancel:
                    e.Cancel = true;
                    return;
            }
        }
    }
}
