﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Eventing.Reader;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RegisterPeople
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    
    public partial class MainWindow : Window
    {
        static List<Person> PersonList = new List<Person>();
        public MainWindow()
        {
            InitializeComponent();
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(OnProcessExit);
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("^[0-9]$");
            e.Handled = !regex.IsMatch(e.Text);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if ((TbName.Text.Length < 2) || (TbName.Text.Length > 50))
            {
                MessageBox.Show("Invalid name, the name length must between 2-50");
            }
            string name = TbName.Text;

            int age;
            Int32.TryParse(TbAge.Text, out age);
            if ((age < 1) || (age > 150))
            {
                MessageBox.Show("Invalid age, the age must between 1-150");
            }

            string gender = null;
            if (RbMale.IsEnabled) gender = "M";
            else if (RbFemale.IsEnabled) gender = "F";
            else if (RbGenderNa.IsEnabled) gender = "N/A";

            List<string> pets = new List<string>();
            if (CbCats.IsChecked ?? false) pets.Add("Cats");
            if (CbDogs.IsChecked ?? false) pets.Add("Dogs");
            if (CbOthers.IsChecked ?? false) pets.Add("Others");
            string strPets = String.Join(", ", pets.ToArray());

            string continent = CbContinent.Text;

            Person p = new Person(name, age, gender, strPets, continent);
            PersonList.Add(p);

            Console.WriteLine(p.ToString());

            ResetAllValue();
        }

        void ResetAllValue()
        {
            TbName.Text = "";
            TbAge.Text = "";
            RbMale.IsChecked = true;
            CbCats.IsChecked = false;
            CbDogs.IsChecked = false;
            CbOthers.IsChecked = false;
            CbContinent.SelectedIndex = 1;
        }

        static void SaveDataToFile()
        {
            try
            {
                StreamWriter file = new StreamWriter(@"..\..\persons.txt");
                foreach (Person p in PersonList)
                {
                    file.WriteLine(p.ToString());
                }
                file.Close();
            }
            catch (IOException e)
            {
                Console.WriteLine("FATAL ERROR: " + e.StackTrace);
            }
        }

        static void OnProcessExit(object sender, EventArgs e)
        {
            Console.WriteLine("I'm out of here");
            SaveDataToFile();
        }
    }

    

    class Person
    {
        private string name;
        private int age;
        private string gender;
        private string pets;
        private string continent;

        public Person(string name, int age, string gender, string pets, string continent)
        {
            this.name = name;
            this.age = age;
            this.gender = gender;
            this.pets = pets;
            this.continent = continent;
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int Age
        {
            get { return age; }
            set { age = value; }
        }

        public string Gender
        {
            get { return gender; }
            set { gender = value; }
        }

        public string Pets
        {
            get { return pets; }
            set { pets = value; }
        }

        public string Continent
        {
            get { return continent; }
            set { continent = value; }
        }

        public override string ToString()
        {
            return string.Format("Name: {0}; Age: {1}; Gender: {2}; Pets: {3}; Continent: {4}", name, age, gender, pets, continent);
        }
    }
}
