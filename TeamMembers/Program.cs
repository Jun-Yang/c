﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamMembers
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, string> teamMembers = new Dictionary<string, string>();
            List<string> nameList = new List<string> { }; 
            // read the string from a file.
            try
            {
                FileStream fileStream = new FileStream(@"..\..\members.txt", FileMode.Open, FileAccess.Read);
                StreamReader streamReader = new StreamReader(fileStream, Encoding.UTF8);
                {
                    string line;
                    while ((line = streamReader.ReadLine()) != null)
                    {
                        char[] splitChars = new[] { ':'};
                        string[] subStr = line.Split(splitChars);
                        teamMembers.Add(subStr[0], subStr[1]);
                    }
                    foreach (KeyValuePair<string, string> item in teamMembers) {
                        char[] splitChars = new[] { ',' };
                        string[] subStr = item.Value.Split(splitChars);
                        for (int i = 0; i < subStr.Count(); i++) {
                            if (!nameList.Contains(subStr[i])) {
                                nameList.Add(subStr[i]);
                            }
                        }
                    }

                    foreach (string name in nameList) {
                        string output = name + " plays in ";
                        foreach (KeyValuePair<string, string> item in teamMembers) {
                            if (item.Value.Contains(name)) {
                                output += item.Key;
                                output += ", ";
                            }
                        }
                        Console.WriteLine(output);
                        Debug.WriteLine(output);
                    }
                    Console.ReadKey();
                }
            }
            catch (IOException e)
            {
                Console.WriteLine("Fatal Error" + e.StackTrace);
            }
        }
    }
}

