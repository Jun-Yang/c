﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz2Notes
{
    class Title
    {
        public Title(string title, string body)
        {
            this.TitleName = title;
            this.Body = body;
            _id = ++instanceCount;
        }

        private static int instanceCount;
        private readonly int _id;
        public int Id
        {
            get
            {
                return _id;
            }
        }

        private string _title;
        public string TitleName
        {
            get
            {
                return _title;
            }
            set
            {
                if (value.Length < 2 || value.Length > 100)
                {
                    throw new ArgumentException("Name must be 2-100 characters long");
                }
                _title = value;
            }
        }

        private string _body;
        public string Body
        {
            get
            {
                return _body;
            }
            set
            {
                if (value.Length < 2 || value.Length > 2000)
                {
                    throw new ArgumentException("Name must be 2-2000 characters long");
                }
                _body = value;
            }
        }
    }
    
}
