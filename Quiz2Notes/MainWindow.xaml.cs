﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace Quiz2Notes
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private bool _fileChanged;
        private string _filename = "c:\\Untitled.txt";
        List<Title> titleList = new List<Title>();

        public MainWindow()
        {
            InitializeComponent();
            TbFilePath.Text = "c:\\Untitled.txt";
            lvTitle.ItemsSource = titleList;
        }
        

        private void MiOpen_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var openFileDialog = new OpenFileDialog
                {
                    Filter = "Text file (*.notes)|*.notes",
                    InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
                };
                if (openFileDialog.ShowDialog() == true)
                {
                    string[] lines = File.ReadAllLines(openFileDialog.FileName);
                    SaveToList(lines);
                    lvTitle.Items.Refresh();
                    _filename = openFileDialog.FileName;
                    TbFilePath.Text = _filename;
                }
                _fileChanged = false;
                TbFileStatus.Text = "";
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("File not found" + ex.StackTrace);
            }
        }

        void SaveToList(string[] lines)
        {
            int i = 0;
            while(i < lines.Length)
            {
                string title = lines[i];
                i++;
                string body = lines[i];
                i++;
                Title t = new Title(title, body);
                titleList.Add(t);
            }
        }
        
        private void MiSave_Click(object sender, RoutedEventArgs e)
        {
            string combindedString = "";
            try
            {
                foreach (Title t in titleList)
                {
                   combindedString  = string.Join("\n", t.TitleName, t.Body);
                   File.AppendAllText(_filename, combindedString);
                   File.AppendAllText(_filename, "\n");
                }
                //File.WriteAllText(_filename, combindedString);
                _fileChanged = false;
                TbFileStatus.Text = "";
                
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("File save error" + ex.StackTrace);
            }
        }
        
        private void MiSaveAs_Click(object sender, RoutedEventArgs e)
        {
            var saveFileDialog = new SaveFileDialog
            {
                Filter = "Text file (*.notes)|*.notex",
                InitialDirectory = @"c:\temp\"
            };
            if (saveFileDialog.ShowDialog() != true) return;
            _filename = saveFileDialog.FileName;
            TbFilePath.Text = _filename;
            MiSave_Click(null, null);
        }

        private void MiExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void txtEditor_TextChanged(object sender, TextChangedEventArgs e)
        {
            _fileChanged = true;
            TbFileStatus.Text = "Modified";
        }


        private void Window_Closing(object sender, CancelEventArgs e)
        {
            if (!_fileChanged) return;
            var result = MessageBox.Show("Do you want to save changes to Untitled?", "MiniNotepad", MessageBoxButton.YesNoCancel);
            switch (result)
            {
                case MessageBoxResult.Yes:
                    MiSave_Click(null, null);
                    break;
                case MessageBoxResult.No:
                    break;
                case MessageBoxResult.Cancel:
                    e.Cancel = true;
                    return;
            }
        }

        private void MiCreate_Click(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            string body = tbBody.Text;
            
            try
            {
                Title t = new Title(name, body);
                titleList.Add(t);
                lvTitle.Items.Refresh();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void MiDelete_Click(object sender, RoutedEventArgs e)
        {
            int index = lvTitle.SelectedIndex;
            if (index < 0)
            {
                return;
            }
            titleList.RemoveAt(index);
            lvTitle.Items.Refresh();
        }

        private void lvTitle_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = lvTitle.SelectedIndex;
            if (index < 0)
            {
                return;
            }
            Title t = titleList[index];
            tbName.Text = t.TitleName;
            tbBody.Text = t.Body + "";
        }
    }
}
